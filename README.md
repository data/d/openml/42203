# OpenML dataset: Duffing_system_phase_transition

https://www.openml.org/d/42203

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is an artificial simulation of the Duffing system with one phase transition to the chaotic regime.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42203) of an [OpenML dataset](https://www.openml.org/d/42203). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42203/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42203/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42203/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

